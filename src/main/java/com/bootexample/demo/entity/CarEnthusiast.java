package com.bootexample.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name="carenthusiast")
public class CarEnthusiast {

    @Id
    //@GeneratedValue
    @Column(name = "ce_id"/*, unique = true,nullable = false*/)
    private UUID ceid;

    //@Column(name = "firstname", nullable = false, length = 20)
    private String firstname;

    //@Column(name = "secondname", nullable = false, length = 20)
    private String secondname;

    //@Column(name = "lastname", length = 20)
    private String lastname;

    @Column(name = "birthdate", nullable = false)
    private LocalDate date;

    @OneToMany(mappedBy = "owner")
    private List<Car> cars;
}




