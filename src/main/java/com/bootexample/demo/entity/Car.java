package com.bootexample.demo.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Getter
@Setter
@Table(name="cars")
public class Car {

    private String model;
    private String colorname;
    private String carregnumber;

    @Id
    @GeneratedValue
    private UUID car_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ce_id", nullable = true)
    private CarEnthusiast owner;
}
