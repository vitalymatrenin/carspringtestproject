package com.bootexample.demo.dto;


import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


public class PieDTO {
        public String name;
        public String amount;
        public int price;


        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getAmount() {
                return amount;
        }

        public void setAmount(String amount) {
                this.amount = amount;
        }

        public int getPrice() {
                return price;
        }

        public void setPrice(int price) {
                this.price = price;
        }
}
