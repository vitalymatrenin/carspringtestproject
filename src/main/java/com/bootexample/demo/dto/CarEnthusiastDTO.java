package com.bootexample.demo.dto;

import lombok.Getter;
import lombok.Setter;
import java.sql.Date;
import java.util.UUID;

@Setter
@Getter
public class CarEnthusiastDTO {
    private String firstName;
    private String secondName;
    private String lastName;
}
