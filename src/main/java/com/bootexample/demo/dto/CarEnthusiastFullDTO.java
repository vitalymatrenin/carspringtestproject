package com.bootexample.demo.dto;

import lombok.Getter;
import lombok.Setter;
import java.sql.Date;
import java.time.LocalDate;
import java.util.UUID;

@Setter
@Getter
public class CarEnthusiastFullDTO {
    private String firstName;
    private String secondName;
    private String lastName;
    private UUID ceId;
    private LocalDate date;
}
