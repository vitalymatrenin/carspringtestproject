package com.bootexample.demo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoApplication {

	private static final Logger logger= LoggerFactory.getLogger(DemoApplication.class);


	public static void main(String[] args) {

		logger.info("Example log from {}", DemoApplication.class.getSimpleName());
		SpringApplication.run(DemoApplication.class, args);
	}

}
