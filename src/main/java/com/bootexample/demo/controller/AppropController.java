package com.bootexample.demo.controller;

import com.bootexample.demo.service.AppropInterface;
import com.bootexample.demo.service.AppropService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@ConditionalOnProperty(prefix = "beans", name = "add", havingValue = "approp")
@RestController
@RequestMapping("/approp")
public class AppropController {

    private final AppropInterface appropInterface;

    public AppropController(
            AppropInterface appropInterface, AppropService appropService) {
        this.appropInterface = appropService;
    }


    @GetMapping("/")
    @ResponseBody
    public ResponseEntity<?> getObject(){
        return new ResponseEntity<>(appropInterface.get(), HttpStatus.OK);
    }

}
