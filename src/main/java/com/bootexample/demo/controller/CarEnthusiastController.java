package com.bootexample.demo.controller;

import com.bootexample.demo.dto.CarEnthusiastDTO;
//import com.bootexample.demo.entity.CarEnthusiast;
import com.bootexample.demo.dto.CarEnthusiastFullDTO;
import com.bootexample.demo.repository.CarEnthusiastRepository;
import com.bootexample.demo.service.CarEnthusiastInterface;
import com.bootexample.demo.service.CarEnthusiastService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/car")
public class CarEnthusiastController {

    private final CarEnthusiastInterface cei;
    private final CarEnthusiastRepository repository;

    public CarEnthusiastController(CarEnthusiastService ces, CarEnthusiastRepository carEnthusiastRepository) {
        cei = ces;
        this.repository = carEnthusiastRepository;
    }

    @GetMapping("/getList")
    public ResponseEntity<?> findAll() {

        return new ResponseEntity<>(cei.findAll(), HttpStatus.OK);
    }


    @GetMapping("/findById/{id}")
    ResponseEntity<?> findById(@PathVariable("id") UUID id) {
        return new ResponseEntity<>(cei.findById(id), HttpStatus.OK);
    }

    @PostMapping("/post")
    ResponseEntity<?> post(@RequestBody CarEnthusiastFullDTO carEnthusiastFullDTO) {
        try {
            cei.save(carEnthusiastFullDTO);
        } catch (Exception e) {
            return new ResponseEntity<>("failed to store:\n " + e.toString(), HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>("successfully stored", HttpStatus.OK);
    }


    @DeleteMapping("/delete/{id}")
    ResponseEntity<?> delete(@PathVariable("id") UUID id) {
        try {
            cei.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<>("failed to delete:\n " + e.toString(), HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>("successfully removed", HttpStatus.OK);
    }

    @PutMapping("/update")
    ResponseEntity<?> update(@RequestBody CarEnthusiastFullDTO carEnthusiastFullDTO) {
        try {
            cei.update(carEnthusiastFullDTO);
        } catch (Exception e) {
            return new ResponseEntity<>("failed to update:\n " + e.toString(), HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>("successfully updated", HttpStatus.OK);
    }

/*    @GetMapping("/query1")
    ResponseEntity<?> getCarOwners() {
        List<CarEnthusiastFullDTO> carEnthusiastFullDTOList;
        try{
            carEnthusiastFullDTOList = cei.getCarOwners();
        }
        catch (Exception e) {
            return new ResponseEntity<>("failed to execute query:\n "+e.toString(),HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>(carEnthusiastFullDTOList,HttpStatus.OK);
    }

    @GetMapping("/query2")
    ResponseEntity<?> getWhiteCarOwners() {
        List<CarEnthusiastFullDTO> carEnthusiastFullDTOList;
        try{
            carEnthusiastFullDTOList = cei.getWhiteCarOwners();
        }
        catch (Exception e) {
            return new ResponseEntity<>("failed to execute query:\n "+e.toString(),HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>(carEnthusiastFullDTOList,HttpStatus.OK);
    }

    @GetMapping("/query3")
    ResponseEntity<?> getCarNonOwners() {
        List<CarEnthusiastFullDTO> carEnthusiastFullDTOList;
            carEnthusiastFullDTOList = cei.getCarNonOwners();
        return new ResponseEntity<>(carEnthusiastFullDTOList,HttpStatus.OK);
    }*/

    @GetMapping("/query{queryNum}")
    ResponseEntity<?> getCarOwnersByCarNum(@PathVariable("queryNum") int queryNum) {
        List<CarEnthusiastFullDTO> carEnthusiastFullDTOList;
        try {
            switch (queryNum) {
                case (1): {
                    carEnthusiastFullDTOList = cei.getCarOwners();
                    break;
                }
                case (2): {
                    carEnthusiastFullDTOList = cei.getWhiteCarOwners();
                    break;
                }
                case (3): {
                    carEnthusiastFullDTOList = cei.getCarNonOwners();
                    break;
                }
                case (4): {
                    carEnthusiastFullDTOList = cei.getCarOwnersByCarNum();
                    break;
                }
                case (5): {
                    carEnthusiastFullDTOList = cei.getPeopleByAge();
                    break;
                }
                case (6): {
                    carEnthusiastFullDTOList = cei.getFirstThreeInList();
                    break;
                }
                case (8): {
                    carEnthusiastFullDTOList = cei.getOwnersOfRedOrWhiteCars();
                    break;
                }
                default:
                    return new ResponseEntity<>("Attempt to execute nonexisting query", HttpStatus.NOT_FOUND);


            }
            return new ResponseEntity<>(carEnthusiastFullDTOList, HttpStatus.OK);

        }
        catch (Exception e) {
            return new ResponseEntity<>("failed to execute query:\n "+e.toString(),HttpStatus.NOT_ACCEPTABLE);
        }


    }

//    @GetMapping("/findbyname/{name}")
//    ResponseEntity<?> findByName(@PathVariable("name") String name) {
//        return new ResponseEntity<>(cei.getDTOListByName(name),HttpStatus.OK);
//    }
//
//    @GetMapping("/")
//    ResponseEntity<?> test() {
//        return new ResponseEntity<String>(cei.get().getFirstname(),HttpStatus.OK);
//
//    }
//
//    @GetMapping("/carowners")
//    ResponseEntity<?> getCarOwnersTest() {
//        return new ResponseEntity<>(cei.getCarOwnersDTO(),HttpStatus.OK);
//
//    }
//   /* public CarEnthusiastDTO getObjectByID (UUID id) {
//        CarEnthusiastDTO tempDTO = new CarEnthusiastDTO();
//
//        tempDTO.getLastName()
//        return cei.get();
//        //TODO
//        return(new CarEnthusiastDTO());
//    }*/
//
//    public void setObject(CarEnthusiastDTO cedtoTemp) {
//        //cei.set(cedtoTemp);
//        //TODO
//        //cei.set(new CarEnthusiast());
//    }


}
