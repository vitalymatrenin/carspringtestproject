package com.bootexample.demo.controller;


import com.bootexample.demo.dto.PieDTO;
import com.bootexample.demo.service.PieInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pie")
public class DemoController {


    private final PieInterface pieInterface;

    public DemoController(
            /*@Autowired
            @Qualifier("pieService")*/
            PieInterface pieInterface) {
        this.pieInterface = pieInterface;
    }


    @PostMapping("/")

    public ResponseEntity<String> setObject(@RequestBody PieDTO pieOrder)    {


        try {
            pieOrder.getName().toString();
            pieInterface.put(pieOrder);
            return new ResponseEntity<String>("pie stored", HttpStatus.OK);
        }
        catch(Exception e)
        {
            return new ResponseEntity<String>("pie not stored", HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @GetMapping("/")
    @ResponseBody
    public ResponseEntity<?> getObject(){
        return new ResponseEntity<>(pieInterface.get(), HttpStatus.OK);
    }

}





