package com.bootexample.demo.configuration;

import com.bootexample.demo.service.PieInterface;
import com.bootexample.demo.service.PieLogService;
import com.bootexample.demo.service.PieService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PieConfiguration {

    @Bean
    public PieInterface pieInterface() {
        return new PieService();
    }





}
