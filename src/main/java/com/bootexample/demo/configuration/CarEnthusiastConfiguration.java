package com.bootexample.demo.configuration;

import com.bootexample.demo.repository.CarEnthusiastRepository;
import com.bootexample.demo.service.CarEnthusiastInterface;
import com.bootexample.demo.service.CarEnthusiastService;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class CarEnthusiastConfiguration {

    @Bean
    public CarEnthusiastInterface carEnthusiastInterface(CarEnthusiastRepository carEnthusiastRepository, ModelMapper modelMapper) {
        return new CarEnthusiastService(modelMapper,carEnthusiastRepository);

    }





}
