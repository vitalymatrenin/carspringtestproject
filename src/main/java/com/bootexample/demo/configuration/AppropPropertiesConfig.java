package com.bootexample.demo.configuration;

import com.bootexample.demo.service.AppropInterface;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(prefix = "beans", name = "add", havingValue = "approp")
@EnableConfigurationProperties(AppropProperties.class)
public class AppropPropertiesConfig {

}
