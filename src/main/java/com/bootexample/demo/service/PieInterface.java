
package com.bootexample.demo.service;

import com.bootexample.demo.dto.PieDTO;

    public interface PieInterface {

    public PieDTO get();
    public void put(PieDTO item);
}
