package com.bootexample.demo.service;
import com.bootexample.demo.dto.CarEnthusiastDTO;
import com.bootexample.demo.dto.CarEnthusiastFullDTO;
import com.bootexample.demo.entity.CarEnthusiast;
import com.bootexample.demo.repository.CarEnthusiastRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import java.time.LocalDate;
import java.util.*;

@Service
@RequiredArgsConstructor


public class CarEnthusiastService implements CarEnthusiastInterface{

//    private CarEnthusiastDTO cedto;


    private final ModelMapper mapper;
    private final CarEnthusiastRepository carEnthusiastRepository;





    @Override
    public CarEnthusiastDTO findById(UUID id) {
        Optional<CarEnthusiast> op = carEnthusiastRepository.findById(id);
        if (op.isEmpty()) {
            return null;
        }
        return mapper.map(op.get(),CarEnthusiastDTO.class);
    }

    @Override
    public CarEnthusiastFullDTO save(CarEnthusiastFullDTO carEnthusiastFullDTO) {
        if (carEnthusiastRepository.existsById(carEnthusiastFullDTO.getCeId()))
            throw new EntityExistsException();
        else
        carEnthusiastRepository.save(mapper.map(carEnthusiastFullDTO,CarEnthusiast.class));
        return null;
    }

    @Override
    public void delete(UUID id) {
        carEnthusiastRepository.deleteById(id);
    }

    @Override
    public List<CarEnthusiastDTO> findAll() {
        List<CarEnthusiast> carEnthusiastTempList = carEnthusiastRepository.findAll();
        List<CarEnthusiastDTO> carEnthusiastDTOTempList = new ArrayList<CarEnthusiastDTO>();
        for (CarEnthusiast ce : carEnthusiastTempList){
            carEnthusiastDTOTempList.add(mapper.map(ce,CarEnthusiastDTO.class));
        }
        return carEnthusiastDTOTempList;
    }

    public List<CarEnthusiastFullDTO> getFDTOListFromEntities(Collection<CarEnthusiast> carEnthusiastTempList){
        List<CarEnthusiastFullDTO> carEnthusiastFullDTOTempList = new ArrayList<CarEnthusiastFullDTO>();
        for (CarEnthusiast ce : carEnthusiastTempList){
            carEnthusiastFullDTOTempList.add(mapper.map(ce,CarEnthusiastFullDTO.class));
        }
        return carEnthusiastFullDTOTempList;
    }

    @Override
    public void update(CarEnthusiastFullDTO carEnthusiastFullDTO) {
        if (carEnthusiastRepository.existsById(carEnthusiastFullDTO.getCeId()))
            carEnthusiastRepository.save(mapper.map(carEnthusiastFullDTO, CarEnthusiast.class));
    }


    @Override
    public List<CarEnthusiastFullDTO> getCarOwners(){
        return getFDTOListFromEntities(carEnthusiastRepository.findAllByCarsIsNotNull());
    }

    @Override
    public List<CarEnthusiastFullDTO> getWhiteCarOwners(){
        return getFDTOListFromEntities(carEnthusiastRepository.findAllByCars_ColornameIgnoreCaseEquals("white"));
    }

    @Override
    public List<CarEnthusiastFullDTO> getCarNonOwners(){
        return getFDTOListFromEntities(carEnthusiastRepository.findAllByCarsIsNull());
    }

    @Override
    public List<CarEnthusiastFullDTO> getCarOwnersByCarNum(){
        return getFDTOListFromEntities(carEnthusiastRepository.findAllByCars_CarregnumberLike("%700%"));
    }

    @Override
    public List<CarEnthusiastFullDTO> getPeopleByAge(){
        return getFDTOListFromEntities(carEnthusiastRepository.findByDateLessThan(LocalDate.now().minusYears(30)));
    }

    @Override
    public List<CarEnthusiastFullDTO> getFirstThreeInList(){
        return getFDTOListFromEntities(carEnthusiastRepository.findTop3ByOrderByFirstnameAsc());
    }


    @Override
    public List<CarEnthusiastFullDTO> getOwnersOfRedOrWhiteCars(){
        //return getFDTOListFromEntities(carEnthusiastRepository.findAllByCars_ColornameIsOrCars_ColornameIs("white","red"));
        return getFDTOListFromEntities(carEnthusiastRepository.findDistinctAllByCars_ColornameIn(Arrays.asList("white","red")));
    }






////  List<CarEnthusiastDTO>
//    @Override
//    public List<CarEnthusiast> getCarOwners(){
//        return(List.copyOf(cerepo.getAllCarOwners()));
//    }
//
//    @Override
//    public List<CarEnthusiastDTO> getCarOwnersDTO(){
//        return(getDTOListFromCollection(cerepo.getTestNonNative()));
//
//        //return(getDTOListFromCollection(cerepo.getAllCarOwnersByJoin()));
//    }
//
//    @Override
//    public List<CarEnthusiastDTO> getDTOListByName(String firstName){
//        return(getDTOListFromCollection(cerepo.findAllByFirstname(firstName)));
//
//        //return(getDTOListFromCollection(cerepo.getAllCarOwnersByJoin()));
//    }
//
//    @Override
//    public List<CarEnthusiastDTO> getDTOListFromCollection(Collection<CarEnthusiast> tempCECollection){
//        List<CarEnthusiastDTO> tempList = new ArrayList<CarEnthusiastDTO>();
//        CarEnthusiastDTO tempCeDTO;
//        for (CarEnthusiast ceTemp : tempCECollection){
//            tempCeDTO=new CarEnthusiastDTO();
//            tempCeDTO.setFirstName(ceTemp.getFirstname());
//            tempCeDTO.setSecondName(ceTemp.getSecondname());
//            tempCeDTO.setLastName(ceTemp.getLastname());
//            tempList.add(tempCeDTO);
//        }
//        return(tempList);
//    }
//
//    /*@Override
//    public CarEnthusiastDTO get(){
//        return new CarEnthusiastDTO();
//    }*/
//
//
//    @Override
//    public void set(CarEnthusiast cedto){
//
//    }
}
