package com.bootexample.demo.service;

import com.bootexample.demo.dto.CarEnthusiastDTO;
import com.bootexample.demo.dto.CarEnthusiastFullDTO;
import com.bootexample.demo.entity.CarEnthusiast;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface CarEnthusiastInterface {

    CarEnthusiastDTO findById(UUID id);
    CarEnthusiastFullDTO save(CarEnthusiastFullDTO carEnthusiastFullDTO);
    void delete(UUID id);
    List<CarEnthusiastDTO> findAll();
    void update(CarEnthusiastFullDTO carEnthusiastFullDTO);
    public List<CarEnthusiastFullDTO> getWhiteCarOwners();

    public List<CarEnthusiastFullDTO> getCarOwners();

    public List<CarEnthusiastFullDTO> getCarNonOwners();

    public List<CarEnthusiastFullDTO> getCarOwnersByCarNum();

    public List<CarEnthusiastFullDTO> getPeopleByAge();

    public List<CarEnthusiastFullDTO> getFirstThreeInList();

//    public List<CarEnthusiastFullDTO> getOwnersOfManyCars();

    public List<CarEnthusiastFullDTO> getOwnersOfRedOrWhiteCars();


    //public void set(CarEnthusiast cedto);
   // public Collection<CarEnthusiast> getCarOwners();
    //public List<CarEnthusiastDTO> getCarOwnersDTO();
    //public List<CarEnthusiastDTO> getDTOListFromCollection(Collection<CarEnthusiast> tempCECollection);
    //public List<CarEnthusiastDTO> getDTOListByName(String firstName);

}
