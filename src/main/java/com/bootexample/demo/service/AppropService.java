package com.bootexample.demo.service;

import com.bootexample.demo.configuration.AppropProperties;
import com.bootexample.demo.dto.AppropCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(prefix = "beans", name = "add", havingValue = "approp")
public class AppropService implements AppropInterface {

    @Autowired
    private AppropProperties appropProperties;

//    @Value("${approp-collection.first-name:}")
//    private String firstName;
//    @Value("${approp-collection.second-name}")
//    private String secondName;
//    @Value("${approp-collection.last-name}")
//    private String lastName;
//    @Value("${approp-collection.age}")
//    private int age;


    @Override
    public AppropCollection get() {
        AppropCollection apc=new AppropCollection();
        apc.setFirstName(appropProperties.getFirstName());
        apc.setSecondName(appropProperties.getSecondName());
        apc.setLastName(appropProperties.getLastName());
        apc.setAge(appropProperties.getAge());

        return apc;
    }

}
