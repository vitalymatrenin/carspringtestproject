package com.bootexample.demo.service;

import com.bootexample.demo.dto.PieDTO;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
//@Profile("test")
public class PieLogService implements PieInterface {

    private PieDTO testPie;

    @Override
    public void put(PieDTO item) {
        this.testPie = item;

    }

    @Override
    public PieDTO get() {
        return testPie;
    }

}
