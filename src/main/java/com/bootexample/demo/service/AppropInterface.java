package com.bootexample.demo.service;

import com.bootexample.demo.dto.AppropCollection;

public interface AppropInterface {
    public AppropCollection get();

}
