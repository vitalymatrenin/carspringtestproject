package com.bootexample.demo.repository;

import com.bootexample.demo.entity.CarEnthusiast;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.*;

public interface CarEnthusiastRepository extends JpaRepository<CarEnthusiast, UUID> {

    @Query(value="select DISTINCT \n" +
            "\tcarEnthusiast.ce_id, firstname, secondname, lastname, birthdate  \n" +
            "\tfrom carEnthusiast,cars \n" +
            "\twhere carEnthusiast.ce_id=cars.ce_id;",nativeQuery = true)
    Collection<CarEnthusiast> getAllCarOwners();

    @Query(value="select DISTINCT carenthusiast.* from carenthusiast join cars on carenthusiast.ce_id = cars.ce_id;",nativeQuery = true)
    Collection<CarEnthusiast> getAllCarOwnersByJoin();

    @Query("select DISTINCT ce from CarEnthusiast ce join Car c on ce.ceid = c.owner")
    Collection<CarEnthusiast> getAllCarOwnersNN();

    List<CarEnthusiast> findAllByCarsIsNotNull();

    @Query("select DISTINCT ce from CarEnthusiast ce join Car c on ce.ceid = c.owner and c.colorname='white'")
    Collection<CarEnthusiast> getAllWhiteCarOwnersNN();

    List<CarEnthusiast> findAllByCars_ColornameIgnoreCaseEquals(String colorName);

    @Query("select DISTINCT ce from CarEnthusiast ce left join Car c on ce.ceid = c.owner where c.owner = null")
    Collection<CarEnthusiast> getAllNonOwnersNN();

    List<CarEnthusiast> findAllByCarsIsNull();

    @Query("select DISTINCT ce from CarEnthusiast ce join Car c on ce.ceid = c.owner and c.carregnumber like '%700%'")
    Collection<CarEnthusiast> getCarOwnersByCarNumNN();

    List<CarEnthusiast> findAllByCars_CarregnumberLike(String carregnum);


    @Query("select  ce from CarEnthusiast ce where extract(year from age(date))>30")
    Collection<CarEnthusiast> getPeopleByAgeNN();

    List<CarEnthusiast> findByDateLessThan(LocalDate date);

    @Query("select ce from CarEnthusiast ce order by ce.firstname")
    Collection<CarEnthusiast> getFirstThreeInListNN();

    List<CarEnthusiast> findTop3ByOrderByFirstnameAsc();

    /*@Query("select cen from CarEnthusiast cen, select ce, count(ce.ceId) from CarEnthusiast join cars c where ce.ceId=c.owner group by ce having count(ce.ceId)>1")
    Collection<CarEnthusiast> getOwnersOfManyCarsNN();*/




    List<CarEnthusiast> findAllByCars_ColornameIsOrCars_ColornameIs(String colorname1, String colorname2);
    List<CarEnthusiast> findDistinctAllByCars_ColornameIn(Collection<String> colorname);



}
